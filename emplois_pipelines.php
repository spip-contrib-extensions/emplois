<?php

/**
 * Utilisations de pipelines par Emplois
 *
 * @plugin     Emplois
 * @copyright  2016
 * @author     Peetdu
 * @licence    GNU/GPL
 * @package    SPIP\Emplois\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Inserer la CSS de emplois si config cochee
 *
 * @param $flux
 * @return $flux
 */
function emplois_insert_head_css($head) {
	include_spip('inc/config');
	$cfg = lire_config('emplois/affichage_public/css_public');
	if ($cfg and $cfg == 'on') {
		$head .= '<link rel="stylesheet" type="text/css" href="' . timestamp(find_in_path('css/emplois.css')) . '" />';
	}

	return $head;
}

/**
 * Ajouter les objets sur les vues de rubriques
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function emplois_affiche_enfants($flux) {
	$e = trouver_objet_exec($flux['args']['exec']);
	if (
		$e
		and $e['type'] == 'rubrique'
		and $e['edition'] == false
	) {
		$id_rubrique = $flux['args']['id_rubrique'];
		$lister_objets = charger_fonction('lister_objets', 'inc');

		$bouton = '';
		if (autoriser('creeroffredans', 'rubrique', $id_rubrique)) {
			$bouton .= icone_verticale(_T('offre:icone_creer_offre'), generer_url_ecrire('offre_edit', "id_rubrique=$id_rubrique"), 'offre-24.png', 'new', 'right')
					. "<br class='nettoyeur' />";
		}

		$flux['data'] .= $lister_objets('offres', ['titre' => _T('offre:titre_offres_rubrique') , 'id_rubrique' => $id_rubrique]);
		$flux['data'] .= $bouton;

		$bouton = '';
		if (autoriser('creercvdans', 'rubrique', $id_rubrique)) {
			$bouton .= icone_verticale(_T('cv:icone_creer_cv'), generer_url_ecrire('cv_edit', "id_rubrique=$id_rubrique"), 'cv-24.png', 'new', 'right')
					. "<br class='nettoyeur' />";
		}

		$flux['data'] .= $lister_objets('cvs', ['titre' => _T('cv:titre_cvs_rubrique') , 'id_rubrique' => $id_rubrique]);
		$flux['data'] .= $bouton;
	}
	// Afficher les offres et les CVs sur la page d'un auteur
	if (
		$e
		and $e['type'] == 'auteur'
		and $e['edition'] == false
	) {
		$id_auteur = $flux['args']['id_objet'];
		$lister_objets = charger_fonction('lister_objets', 'inc');

		// Les offres d'emploi
		$flux['data'] .= $lister_objets('offres', ['titre' => _T('offres:info_offres_auteur') , 'id_auteur' => $id_auteur, 'par' => 'date_debut']);
		$flux['data'] .= "<br class='nettoyeur' />";

		// Les CVs
		$flux['data'] .= $lister_objets('cvs', ['titre' => _T('cv:info_cvs_auteur') , 'id_auteur' => $id_auteur, 'par' => 'date_debut']);
		$flux['data'] .= "<br class='nettoyeur' />";
	}

	return $flux;
}


/**
 * Ajout de liste sur la vue d'un auteur
 *
 * @pipeline affiche_auteurs_interventions
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function emplois_affiche_auteurs_interventions($flux) {
	if ($id_auteur = intval($flux['args']['id_auteur'])) {
		$flux['data'] .= recuperer_fond('prive/objets/liste/cvs', [
			'id_auteur' => $id_auteur,
			'titre' => _T('cv:info_cvs_auteur')
		], ['ajax' => true]);
	}
	return $flux;
}


/**
 * Compter les enfants d'une rubrique
 *
 * @pipeline objets_compte_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function emplois_objet_compte_enfants($flux) {
	if ($flux['args']['objet'] == 'rubrique' and $id_rubrique = intval($flux['args']['id_objet'])) {
		// juste les publiés ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['offres'] = sql_countsel('spip_offres', 'id_rubrique = ' . intval($id_rubrique) . " AND (statut = 'publie')");
		} else {
			$flux['data']['offres'] = sql_countsel('spip_offres', 'id_rubrique = ' . intval($id_rubrique) . " AND (statut <> 'poubelle')");
		} 		// juste les publiés ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['cvs'] = sql_countsel('spip_cvs', 'id_rubrique = ' . intval($id_rubrique) . " AND (statut = 'publie')");
		} else {
			$flux['data']['cvs'] = sql_countsel('spip_cvs', 'id_rubrique = ' . intval($id_rubrique) . " AND (statut <> 'poubelle')");
		}
	}
	return $flux;
}

/**
 * Synchroniser la valeur de id secteur
 *
 * @pipeline trig_propager_les_secteurs
 * @param  string $flux Données du pipeline
 * @return string       Données du pipeline
**/
function emplois_trig_propager_les_secteurs($flux) {

	// synchroniser spip_offres
	$r = sql_select(
		'A.id_offre AS id, R.id_secteur AS secteur',
		'spip_offres AS A, spip_rubriques AS R',
		'A.id_rubrique = R.id_rubrique AND A.id_secteur <> R.id_secteur'
	);
	while ($row = sql_fetch($r)) {
		sql_update('spip_offres', ['id_secteur' => $row['secteur']], 'id_offre=' . $row['id']);
	}

	// synchroniser spip_cvs
	$r = sql_select(
		'A.id_cv AS id, R.id_secteur AS secteur',
		'spip_cvs AS A, spip_rubriques AS R',
		'A.id_rubrique = R.id_rubrique AND A.id_secteur <> R.id_secteur'
	);
	while ($row = sql_fetch($r)) {
		sql_update('spip_cvs', ['id_secteur' => $row['secteur']], 'id_cv=' . $row['id']);
	}

	return $flux;
}

/**
 * Gestion de l'ajout d'un document depuis le back-office dans les offres
 * - Insertion : on force le statut du document à "publie" et on copie l'id_document dans la table spip_offres
 * - suppression : on met la valeur "0" dans la champ id_document_offre de la table spip_offres
 *
 * @pipeline post_edition_lien
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function emplois_post_edition_lien($flux) {

	if ($flux['args']['table_lien'] == 'spip_documents_liens' and $flux['args']['objet'] == 'offre') {
		$id_doc = $flux['args']['id_objet_source'];
		$id_offre = $flux['args']['id_objet'];

		if ($flux['args']['action'] == 'insert') {
			// forcer le statut du document à "publié"
			sql_updateq('spip_documents', ['statut' => 'publie'], 'id_document=' . intval($id_doc));

			// copier l'id_document dans la table spip_offres
			sql_updateq('spip_offres', ['id_document_offre' => $id_doc], 'id_offre =' . intval($id_offre));
		}

		if ($flux['args']['action'] == 'delete') {
			// en cas de suppression, mettre l'id_document à "0" dans la table spip_offres
			sql_updateq('spip_offres', ['id_document_offre' => '0'], 'id_offre =' . intval($id_offre));
		}
	}

	return $flux;
}


/**
 * Gestion des Notifications
 * pour envoi d'une notification au webmaster lors d'un dépôt (offre ou CV)
 * on leve un flag a l'insertion
 *
 * @pipeline post_insertion
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function emplois_post_insertion($flux) {
	if (
		isset($flux['args']['table'])
		and in_array($flux['args']['table'], ['spip_offres', 'spip_cvs'])
		and $id = $flux['args']['id_objet']
		and !test_espace_prive()
	) {
		// on leve un flag qui sera vu en post-edition pour lancer la notification quand l'objet est renseigné
		// (sinon ici il est vide encore)
		if (empty($GLOBALS['flag_emplois_depot'])) {
			$GLOBALS['flag_emplois_depot'] = [];
		}
		$objet = objet_type($flux['args']['table']);
		$GLOBALS['flag_emplois_depot']["$objet$id"] = true;
	}

	return $flux;
}

/**
 * Gestion des Notifications
 * pour envoi d'une notification au webmaster lors d'un dépôt (offre ou CV)
 * on notifie en post-edition
 *
 * @pipeline post_edition
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function emplois_post_edition($flux) {
	if (
		isset($flux['args']['table'])
		and in_array($flux['args']['table'], ['spip_offres', 'spip_cvs'])
		and $flux['args']['action'] === 'modifier'
		and $objet = objet_type($flux['args']['table'])
		and $id = $flux['args']['id_objet']
		and !empty($GLOBALS['flag_emplois_depot']["$objet$id"])
	) {

		/******************* NOTIFICATION *******************/
		if ($notifications = charger_fonction('notifications', 'inc')) {
			$notifications(
				'depot' . $objet,
				$id,
				['objet' => $objet]
			);
		}
	}
	return $flux;
}

/**
 * Gestion de la Modération et Notification à la CREATION d'une offre d'emploi ou d'un CV
 *
 * @pipeline formulaire_traiter
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function emplois_formulaire_traiter($flux) {
	$form = $flux['args']['form'];
	$emplois = ['editer_offre', 'editer_cv'];

	if (
		in_array($form, $emplois) // si c'est bien le depot d'une offre d’emploi ou d'un CV
		and !is_numeric($flux['args']['args'][0]) // et que c'est une création
		and !test_espace_prive()
	) {
		// et que c'est un dépot dans l'espace public
		include_spip('inc/config');
		$objet = substr($form, 7);
		$id_table_objet =  id_table_objet($objet);
		$id = $flux['data'][$id_table_objet];

		/******************* MODÉRATION  *******************/
		/* En fonction de l'option choisie dans la config, on institut le bon statut */
		$moderation = lire_config('emplois/fonctionnement_public/moderation', 'priori');
		switch ($moderation) {
			case 'priori':
				$statut = 'prop';
				$message_moderation = _T('emplois:info_depot_moderation_apriori');
				break;
			case 'posteriori':
			default:
				$statut = 'publie';
				$message_moderation = _T('emplois:info_depot_moderation_aposteriori');
				break;
		}
		include_spip('action/editer_objet');

		objet_instituer($objet, $id, ['statut' => $statut]);

		/******************* MESSAGE APRÈS VALIDATION *******************/
		$message_non_inscrit = '';

		/* Si auteur connecté*/
		if (!lire_config('emplois/inscription')) {
			$message_non_inscrit = _T('emplois:info_depot_non_inscrit');
		}

		// Message de retour de validation
		$flux['message_ok'] = _T('emplois:info_depot_enregistre');
		if ($message_non_inscrit) {
			$flux['message_ok'] .= "\n<br/>$message_non_inscrit";
		}
		if ($message_moderation) {
			$flux['message_ok'] .= "\n<br/>$message_moderation";
		}

		$flux['message_ok'] .= "\n<br/><br/>" . _T('emplois:info_depot_merci');
	}

	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function emplois_optimiser_base_disparus($flux) {

	$mydate = sql_quote(trim($flux['args']['date'], "'"));
	sql_delete('spip_offres', "statut='poubelle' AND maj < $mydate");
	sql_delete('spip_cvs', "statut='poubelle' AND maj < $mydate");

	return $flux;
}
