<?php

/**
 * Fonctions utiles au plugin Emplois
 *
 * @plugin     Emplois
 * @copyright  2016
 * @author     Peetdu
 * @licence    GNU/GPL
 * @package    SPIP\Emplois\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @return array
 */
function emplois_lister_destinataires_notifications() {
	$destinataires = [];
	/*************************** Destinataire(s) ***************************/
	// en fonction de l'option choisie dans la configuration des notifications du plugin,
	// récupérer le ou les emails des destinataires
	if (!$notifier_qui = lire_config('emplois/notification')) {
		return $destinataires;
	}

	switch ($notifier_qui) {
		case 'webmaster':
			$destinataires[] = lire_config('email_webmaster');
			break;
		case 'administrateur':
			$destinataires[] = lire_config('emplois/administrateur_email');
			break;
		default:
			$destinataires = sql_allfetsel('email', 'spip_auteurs', 'statut=' . sql_quote('0minirezo'));
			$destinataires = array_column($destinataires, 'email');
			$destinataires = array_filter($destinataires);
			$destinataires = array_unique($destinataires);
			break;
	}

	return $destinataires;
}
