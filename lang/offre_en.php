<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_offre' => 'Add this job',

	// C
	'champ_date_fin_label' => 'Job‘s closing date',
	'champ_texte_offre_label' => 'Job‘s offer text',
	'champ_email_label' => 'Email',
	'champ_emetteur_label' => 'Applicant',
	'champ_id_document_offre_label' => 'id_document_job',
	'champ_nom_label' => 'Name',
	'champ_signature_label' => 'First name and Name',
	'champ_telephone_label' => 'Phone',
	'champ_telephone_label_cours' => 'Phone: ',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_offre' => 'Confirm this job deletion ?',

	'erreur_technique_enregistrement' => 'An error occurs during file saving',

	// I
	'icone_creer_offre' => 'Create a job',
	'icone_modifier_offre' => 'Modify this job',
	'info_1_offre' => 'One job',
	'info_aucun_offre' => 'No job‘s offer',
	'info_aucun_offre_a_valider' => 'No job‘s offer to validate',
	'info_aucun_offre_refuse' => 'No job‘s offer refused',
	'info_nb_offres' => '@nb@ jobs offer',
	'info_nom' => 'Applicant name',
	'info_offres_auteur' => 'This author job‘s offer',
	'info_texte_offre' => 'Job‘s offer text',

	// L
	'legende_deposant' => 'Applicant',
	'legende_description_offre' => 'Job‘s offer description',

	// R
	'retirer_lien_offre' => 'Remove this job offer',
	'retirer_tous_liens_offres' => 'Remove all jobs offer',

	// S
	'supprimer_offre' => 'Delete this job offer',

	// T
	'texte_ajouter_offre' => 'Add a job',
	'texte_changer_statut_offre' => 'This job is:',
	'texte_creer_associer_offre' => 'Create and associate a job',
	'texte_definir_comme_traduction_offre' => 'This job is a translation of job number :',
	'titre_langue_offre' => 'Job‘s language',
	'titre_logo_offre' => 'Job‘s logo',
	'titre_offre' => 'Job offer',
	'titre_offres' => 'Jobs offer',
	'titre_offres_rubrique' => 'Section job‘s offer',
);