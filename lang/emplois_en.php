<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explanation for this exemple',
	'cfg_label_class_fieldset_deposant' => 'Applicant fieldset CSS Class',
	'cfg_label_class_fieldset_description' => 'Description fieldset CSS Class',
	'cfg_label_placeholder' => 'Display the placeholder',
	'cfg_label_activer_cvs' => 'Activate CVs ?',
	'cfg_label_activer_offres' => 'Activate Jobs ?',
	'champ_joindre_pdf_label' => 'Upload a PDF',

	// E
	'emplois_titre' => 'Jobs',
	'entree_prenom_nom' => 'Your first and last name',
	'entree_prenom_nom_2' => 'First + Last name',

	// I
	'info_by' => 'by',

	// L
	'label_offre' => 'job‘s offer',
	'label_format_pdf' => 'PDF file only',

	// O
	'onglet_offres' => 'Jobs',
	'onglet_cvs' => 'CVs',
	'onglet_affichage_public' => 'Front office options',

	// S
	'supprimer' => 'Delete',

	// T
	'texte_statut_a_valider' => 'Submitted for evaluation',
	'titre_page_configurer_emplois' => 'Jobs',
	'cfg_titre_configurer_affichage_public' => 'Displaying forms online',
	'cfg_titre_configurer_offres_contenus' => 'Jobs Content',
	'cfg_titre_configurer_cvs_contenus' => 'CVs content',
	'cfg_titre_configurer_cvs' => 'CVs',
	'cfg_titre_configurer_offres' => 'Jobs',
	'cfg_legend_contenu_offre_deposant' => 'Applicant',
	'cfg_legend_contenu_offre_description_offre' => 'Job‘s description',
	'cfg_legend_joindre_pdf' => 'Authorize PDF upload',

	// V
	'votre' => 'Your',
	
);