<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_cv' => 'Add this CV',

	// C
	'champ_id_auteur_label' => 'id_auteur',
	'champ_id_document_cv_label' => 'id_document_cv',
	'champ_nom_label' => 'Name',
	'confirmer_supprimer_cv' => 'Confirm this CV deletion ?',

	// I
	'icone_creer_cv' => 'Create a CV',
	'icone_modifier_cv' => 'Modify this CV',
	'info_1_cv' => 'One CV',
	'info_aucun_cv' => 'No CV',
	'info_aucun_cv_a_valider' => 'No CV to validate',
	'info_aucun_cv_refuse' => 'No CV refused',
	'info_cvs_auteur' => 'This author CV‘s',
	'info_nb_cvs' => '@nb@ CV',

	// R
	'retirer_lien_cv' => 'Remove this CV',
	'retirer_tous_liens_cvs' => 'Remove all CVs',

	// S
	'supprimer_cv' => 'Delete this CV',

	// T
	'texte_ajouter_cv' => 'Add CV',
	'texte_changer_statut_cv' => 'This CV is:',
	'texte_creer_associer_cv' => 'Create and associate a CV',
	'texte_definir_comme_traduction_cv' => 'This CV is a translation of CV number:',
	'titre_cv' => 'CV',
	'titre_cvs' => 'CVs',
	'titre_cvs_rubrique' => 'CV of this section',
	'titre_langue_cv' => 'CV language',
	'titre_logo_cv' => 'CV logo',
);