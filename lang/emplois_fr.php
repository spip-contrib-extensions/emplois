<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_label_class_fieldset_deposant' => 'Class CSS pour le fieldset Déposant',
	'cfg_label_class_fieldset_description' => 'Class CSS pour le fieldset Description',
	'cfg_label_placeholder' => 'Afficher le placeholder',
	'cfg_affichage_emplois' => 'Affichage des offres d’emplois et CVs',
	'cfg_titre_configurer_fonctionnement_public' => 'Mode de fonctionnement par défaut du dépôt des offres d’emploi et des CVs',
	'cfg_label_case_activer_css' => 'Activer la feuille de style pour la partie publique ?',
	'cfg_label_activer_cvs' => 'Activer les CVs ?',
	'cfg_label_activer_offres' => 'Activer les Offres d‘emplois ?',
	'cfg_label_moderation' => 'Modération',
	'cfg_label_depot_inscription' => 'Dépôt sur inscription',
	'champ_joindre_pdf_label' => 'Joindre un PDF',
	'champ_joindre_pdf_explication' => 'Pouvoir joindre un PDF depuis le formulaire publique',

	// B
	'btn_publication_immediate' => 'Publication immédiate',
	'btn_publication_immediate_explication' => 'Les administrateurs peuvent ensuite refuser ou supprimer les dépôts.',
	'btn_moderation_priori' 	=> 'Modération à priori',
	'btn_moderation_priori_explication' => 'Les dépôts ne s’affichent publiquement qu’après validation par les administrateurs.',
	'btn_inscription_obligatoire' => 'Inscription obligatoire',
	'btn_inscription_obligatoire_explication' => 'Les déposants inscrits peuvent gérer ensuite leurs dépots.',

	// E
	'emplois_titre' => 'Emplois',
	'entree_prenom_nom' => 'Votre prénom et nom',
	'entree_prenom_nom_2' => 'Prénom + Nom',

	'erreur_technique_enregistrement' => 'Erreur lors de l\'enregistrement du fichier',
	'erreur_format_fichier_extension' => 'Vous devez choisir un fichier au format @extension@',

	'formule_politesse_notification' => 'Bien cordialement,',

	// I
	'info_by' => 'par',
	'info_depot_enregistre' => 'Votre dépôt vient d\'être enregistré.',
	'info_depot_moderation_apriori' => 'Le modérateur vous informera rapidement de la validation ou non de votre dépôt.',
	'info_depot_moderation_aposteriori' => "Il est d’ores et déjà publié en ligne.",
	'info_depot_non_inscrit' => "Vous pouvez vérifier / modifier cette offre tant que vous restez sur cette page.",
	'info_depot_merci' => 'Merci et à bientôt.',

	// L
	'label_cv' => 'cv',
	'label_offre' => 'offre',
	'label_format_pdf' => 'format PDF',

	// N
	'notification_config_erreur_email_webmestre' => 'Vous devez renseigner l’adresse email du webmestre dans la configuration de l’identité du site',
	'notification_config_erreur_email_valide' => 'Vous devez saisir une adresse email valide',
	'notification_config_erreur_email_inconnu' => 'Veuillez fournir l’adresse email d’un administrateur du site',
	'notification_aucun' => 'Aucun',
	'notification_demande_validation' => 'Ce nouveau dépôt attend votre validation pour être vu en ligne.',
	'notification_destinataires' => 'Destinataire :',
	'notification_explication' => 'Au dépôt d’une offre d’emploi ou d’un CV, une notification peut être envoyée.',
	'notification_notice_webmaster' => 'L’email du webmestre n’a pas été renseignée dans la configuration de l’',
	'notification_new_offer' => 'Une nouvelle offre d’emploi vient d’être déposée.',
	'notification_new_cv' => 'Un nouveau CV vient d’être déposé.',
	'notification_webmaster' => 'Le webmestre du site',
	'notification_administrateur' => 'Un administrateur du site',

	// O
	'onglet_offres' => 'Offres',
	'onglet_cvs' => 'CVs',
	'onglet_affichage_public' => 'Affichage espace public',

	// S
	'supprimer' => 'Supprimer',

	// T
	'texte_statut_a_valider' => 'À valider',
	'titre_page_configurer_emplois' => 'Emplois',
	'cfg_titre_configurer_affichage_public' => 'Affichage des formulaires dans l‘espace public',
	'cfg_titre_configurer_offres_contenus' => 'Contenu des offres d‘emploi',
	'cfg_titre_configurer_cvs_contenus' => 'Contenu des CVs',
	'cfg_titre_configurer_cvs' => 'LES CVs',
	'cfg_titre_configurer_offres' => 'LES OFFRES D‘EMPLOIS',
	'cfg_legend_contenu_offre_deposant' => 'Déposant',
	'cfg_legend_contenu_offre_description_offre' => 'Description de l‘offre',
	'cfg_legend_joindre_pdf' => 'Autoriser l‘ajout d‘un fichier PDF',

	// V
	'votre' => 'Votre',
	
);