<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'emplois_description' => 'Ce plugin comporte deux objets :

	- <b>Offre d\'emploi</b>
	- <b>CV</b>

	Il est possible d\'activer l\'un ou l\'autre, ou les deux selon vos besoins.

	- <b>depuis l\'espace public</b>
	Des fonctionnalités ont été mises en place afin de faciliter l\'utilisation des formulaires correspondants depuis l\'espace publique, dont notamment la possibilité pour le déposant de joindre un fichier PDF.',
	'emplois_nom' => 'Emplois',
	'emplois_slogan' => 'Gestion de dépôts de CV et d\'offre d\'emploi',
);