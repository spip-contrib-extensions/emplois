<?php

/**
 * Notifications plugin EMPLOIS
 *
 * @plugin     EMPLOIS
 * @copyright  2016
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\emplois\Notifications
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Gestion des Notifications
 * Notifier le déposant si son dêpot a été publié ou refusé
 *
 * @param string $quoi
 * @param int $id_cv
 * @param array $options
 * @return void
 */
function notifications_instituercv_dist($quoi, $id_cv, $options) {

	// ne devrait jamais se produire
	if ($options['statut'] == $options['statut_ancien']) {
		spip_log("$quoi #$id_cv statut inchange", 'notifications');

		return;
	}

	include_spip('inc/texte');

	$modele = '';
	if ($options['statut'] == 'publie') {
			$modele = 'notifications/cv_publie';
	}

	if ($options['statut'] == 'prop' and $options['statut_ancien'] != 'publie') {
		$modele = 'notifications/cv_propose';
	}

	if ($options['statut'] == 'refuse' and $options['statut_ancien'] != 'poubelle') {
		$modele = 'notifications/cv_refuse';
	}

	if ($modele and trouver_fond($modele)) {
		$destinataires = [];
		$id_auteur = sql_getfetsel('id_auteur', 'spip_cvs', 'id_cv=' . intval($id_cv));

		$email_deposant = sql_getfetsel('email', 'spip_auteurs', 'id_auteur=' . intval($id_auteur));
		$destinataires[] = $email_deposant;

		$destinataires = pipeline(
			'notifications_destinataires',
			[
				'args' => ['quoi' => $quoi, 'id' => $id_cv, 'options' => $options],
				'data' => $destinataires
			]
		);

		$texte = email_notification_objet($id_cv, $quoi, $modele);
		/*************************** Envoi ***************************/
		// Note : le paramètre $from est géré dans la config du plugin Facteur
		notifications_envoyer_mails($destinataires, $texte);
	}
}
