<?php

/**
 * Notifications plugin EMPLOIS
 *
 * @plugin     EMPLOIS
 * @copyright  2016
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\emplois\Notifications
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Composition et Envoi  de la notification
 *
 * @param string $quoi
 *   Événement de notification
 * @param int $id
 *     Identifiant de l’offre ou du CV
 * @param array $option
 *     liste d'option
 */
function notifications_depotoffre_dist($quoi, $id, $options) {
	include_spip('inc/texte');
	include_spip('inc/config');
	include_spip('inc/notifications');
	include_spip('inc/emplois');

	/*************************** Destinataire(s) ***************************/
	$destinataires = emplois_lister_destinataires_notifications();
	// en fonction de l'option choisie dans la configuration des notifications du plugin,
	// récupérer le ou les emails des destinataires
	if (empty($destinataires)) {
		return false;
	}

	// donner aux plugins la possibilité d'ajouter des destinataires
	$destinataires = pipeline(
		'notifications_destinataires',
		[
			'args' => ['quoi' => $quoi, 'id' => $id, 'options' => $options],
			'data' => $destinataires
		]
	);

	/*************************** Sujet et texte ***************************/
	$objet = $options['objet'] ?? 'offre';

	// Texte
	$modele = "notifications/depot_$objet";
	$texte = email_notification_objet($id, $objet, $modele);

	/*************************** Envoi ***************************/
	// Note : le paramètre $from est géré dans la config du plugin Facteur
	notifications_envoyer_mails($destinataires, $texte);
}
