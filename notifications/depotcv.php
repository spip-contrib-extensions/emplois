<?php

/**
 * Notifications plugin EMPLOIS
 *
 * @plugin     EMPLOIS
 * @copyright  2016
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\emplois\Notifications
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Composition et Envoi  de la notification
 *
 * @param string $quoi
 *   Événement de notification
 * @param int $id
 *     Identifiant de l’offre ou du CV
 * @param array $option
 *     liste d'option
 */
function notifications_depotcv_dist($quoi, $id, $options) {

	$depotoffre = charger_fonction('depotoffre', 'notifications');
	$options['objet'] = 'cv';
	$depotoffre($quoi, $id, $options);
}
