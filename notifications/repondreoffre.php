<?php

/**
 * Notifications plugin EMPLOIS
 *
 * @plugin     EMPLOIS
 * @copyright  2016
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\emplois\Notifications
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Gestion des Notifications
 * Notifier le déposant si son dêpot a été publié ou refusé
 *
 * @param string $quoi
 * @param int $id_cv
 * @param array $options
 * @return void
 */
function notifications_repondreoffre_dist($quoi, $id_offre, $options) {

	if (
		empty($options['email_from'])
		or empty($options['message'])
	) {
		return;
	}

	$offre = sql_fetsel('nom, email,titre', 'spip_offres', 'id_offre=' . sql_quote($id_offre));
	if (!$offre) {
		return;
	}

	$modele = 'notifications/reponse_offre';

	$envoyer_mail = charger_fonction('envoyer_mail', 'inc'); // pour nettoyer_titre_email
	$contexte = [
		'id_offre' => $id_offre,
		'id' => $id_offre,
		'from' => $options['email_from'],
		'message' => $options['message'],
	];
	$texte = recuperer_fond($modele, $contexte);


	include_spip('inc/emplois');
	$destinataires = emplois_lister_destinataires_notifications();

	// ajouter aussi le deposant de l'offre
	$destinataires[] = $offre['email'];

	// donner aux plugins la possibilité d'ajouter des destinataires
	$destinataires = pipeline(
		'notifications_destinataires',
		[
			'args' => ['quoi' => $quoi, 'id' => $id_offre, 'options' => $options],
			'data' => $destinataires
		]
	);

	if (!empty($destinataires)) {
		/*************************** Envoi ***************************/
		// Note : le paramètre $from est géré dans la config du plugin Facteur
		notifications_envoyer_mails($destinataires, $texte);
	}
}
