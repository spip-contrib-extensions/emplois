<?php

/**
 * Gestion du formulaire de configuration des notifications
 *
 * @plugin     Emplois
 * @copyright  2016
 * @author     Peetdu
 * @licence    GNU/GPL
 * @package    SPIP\Emplois\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/* Vérifier */
function formulaires_configurer_emplois_notifications_verifier_dist() {
	$erreurs = [];
	$notification = _request('notification');
	$administrateur_email = _request('administrateur_email');

	if ($notification == 'webmaster') {
		if (!lire_config('email_webmaster')) {
			$erreurs['notification'] = _T('emplois:notification_config_erreur_email_webmestre');
		}
	}

	if ($notification == 'administrateur') {
		if (!$administrateur_email) {
			$erreurs['notification'] = _T('emplois:notification_config_erreur_email_valide');
		} else {
			$r = sql_getfetsel('id_auteur', 'spip_auteurs', 'email=' . sql_quote($administrateur_email) . ' and statut="0minirezo"');
			if (!$r) {
				$erreurs['notification'] = _T('emplois:notification_config_erreur_email_inconnu');
			}
		}
	}

	return $erreurs;
}
