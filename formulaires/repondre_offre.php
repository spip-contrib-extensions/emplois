<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_repondre_offre_charger_dist($id_offre) {

	$offre = sql_fetsel('titre', 'spip_offres', 'id_offre=' . intval($id_offre));
	if (!$offre) {
		return false;
	}

	$valeurs = [
		'email_message_auteur' => '',
		'sujet_message_auteur' => "Réponse à l\'offre N&ordm;$id_offre",
		'texte_message_auteur' => '',
		'id_offre' => $id_offre,
		'_titre' => $offre['titre'],
		'_nospam_encrypt' => true,
	];
	return $valeurs;
}

function formulaires_repondre_offre_verifier_dist($id_offre) {
	$erreurs = [];
	include_spip('inc/filtres');

	if (!$adres = _request('email_message_auteur')) {
		$erreurs['email_message_auteur'] = _T('info_obligatoire');
	} elseif (!email_valide($adres)) {
		$erreurs['email_message_auteur'] = _T('form_prop_indiquer_email');
	} else {
		include_spip('inc/session');
		session_set('email', $adres);
	}
	if (!$texte = _request('texte_message_auteur')) {
		$erreurs['texte_message_auteur'] = _T('info_obligatoire');
	} elseif (!(strlen($texte) > 10)) {
		$erreurs['texte_message_auteur'] = _T('forum_attention_dix_caracteres');
	}

	return $erreurs;
}

function formulaires_repondre_offre_traiter_dist($id_offre) {

	$options = [
		'email_from' => _request('email_message_auteur'),
		'message' => _request('texte_message_auteur'),
	];

	$res = [
		'message_ok' => _T('form_prop_message_envoye'),
		'editable' => false,
	];

	include_spip('inc/nospam');
	if (function_exists('nospam_confirm_action_html')) {
		$html_confirm = nospam_confirm_action_html('repondreoffre', "repondreoffre #$id_offre de " . $options['email_from'], ['offre', $id_offre, $options], 'notifications/');
		$res['message_ok'] .= $html_confirm;
	}
	else {
		$repondreoffre = charger_fonction('repondreoffre', 'notifications/');
		$repondreoffre('offre', $id_offre, $options);
	}

	return $res;
}
