<?php

/**
 * Fonctions utiles au plugin Emplois
 *
 * @plugin     Emplois
 * @copyright  2016
 * @author     Peetdu
 * @licence    GNU/GPL
 * @package    SPIP\Emplois\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * afficher le placeholder dans le formulaire de l'espace publique
 *
 * @param null|string $arg
 *     Clé des arguments. En absence utilise l'argument
 *     de l'action sécurisée.
 * @return bool
 */
function emplois_afficher_public($attribut) {
	include_spip('inc/config');
	$val = lire_config('emplois/affichage_public/placeholder');
	if (!test_espace_prive() and $val == 'oui') {
		return true;
	}
	return false;
}

/**
 * Récupérer l'id du CV si l'auteur en en déjà déposé un
 *
 * @param int $id_auteur
 * @return string|int
 *	new si pas de CV, $id du CV sinon
 */
function emplois_get_id_cv($id_auteur) {
	$id_cv = sql_getfetsel('id_cv', 'spip_cvs', 'id_auteur=' . intval($id_auteur));
	if (is_null($id_cv)) {
		$id_cv = 'new';
	}
	return $id_cv;
}

/**
 * Savoir si un PDF a déjà été associé au CV
 *
 * @param int $id_auteur
 * @return bool
 */
function emplois_pdf_deja_depose($id_auteur) {
	$id_document_cv = sql_getfetsel('id_document_cv', 'spip_cvs', "id_auteur=$id_auteur");
	if (!is_null($id_document_cv)) {
		return true;
	}
	return false;
}


/**
 * {offres_en_cours}
 * {offres_en_cours #ENV{date}}
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 */
function critere_offres_en_cours_dist($idb, &$boucles, $crit) {

	$boucle = &$boucles[$idb];
	$id_table = $boucle->id_table;
	$_dateref = emplois_calculer_date_reference($idb, $boucles, $crit);
	$_date_debut = "$id_table.date_debut";
	$_date_fin = "$id_table.date_fin";
// si on ne sait pas si les heures comptent, on utilise toute la journee.
	// sinon, on s'appuie sur le champ 'illimite=oui'
	// pour savoir si les dates utilisent les heures ou pas.
	$where =
		["'AND'",
			["'<='", "'$_date_debut'", "sql_quote(date('Y-m-d H:i:59'))"],
			["'>='", "'$_date_fin'", "sql_quote(date('Y-m-d H:i:00'))"]
		];
	if ($crit->not) {
		$where = ["'NOT'", $where];
	}
	$boucle->where[] = $where;
}

/**
 * Fonction privee pour mutualiser de code des criteres_publicite*
 * Retourne le code php pour obtenir la date de reference de comparaison
 * des offres a trouver
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 *
 * @return string code PHP concernant la date.
 **/
function emplois_calculer_date_reference($idb, &$boucles, $crit) {

	if (isset($crit->param[0])) {
		return calculer_liste($crit->param[0], [], $boucles, $boucles[$idb]->id_parent);
	} else {
		return "date('Y-m-d H:i:00')";
	}
}
